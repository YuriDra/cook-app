import { Component } from '@angular/core';
import { Recipe } from '../recepies.model';

@Component({
  selector: 'app-recipes-list',
  templateUrl: './recipes-list.component.html',
  styleUrl: './recipes-list.component.scss'
})

export class RecipesListComponent {
  recipes: Recipe[] = [
    new Recipe('Schabowy', 'kotlet w panierce', 'https://upload.wikimedia.org/wikipedia/commons/0/03/Schabowy_Gniezno.jpg'),
    new Recipe('Schabowy', 'kotlet w panierce', 'https://upload.wikimedia.org/wikipedia/commons/0/03/Schabowy_Gniezno.jpg')
  ];
}
